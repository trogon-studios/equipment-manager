<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%container}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $container_type_id
 *
 * @property ContainerType $containerType
 * @property Device[] $devices
 */
class Container extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%container}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'container_type_id'], 'required'],
            [['container_type_id'], 'integer'],
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'container_type_id' => Yii::t('app', 'Container type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContainerType()
    {
        return $this->hasOne(ContainerType::className(), ['id' => 'container_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevices()
    {
        return $this->hasMany(Device::className(), ['container_id' => 'id']);
    }
}
