<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%device}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $model
 * @property integer $container_id
 * @property integer $device_type_id
 *
 * @property Container $container
 * @property DeviceType $deviceType
 * @property DeviceInfo[] $deviceInfos
 */
class Device extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%device}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'model', 'device_type_id'], 'required'],
            [['container_id', 'device_type_id'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['model'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'model' => Yii::t('app', 'Model'),
            'container_id' => Yii::t('app', 'Container'),
            'device_type_id' => Yii::t('app', 'Device type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContainer()
    {
        return $this->hasOne(Container::className(), ['id' => 'container_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeviceType()
    {
        return $this->hasOne(DeviceType::className(), ['id' => 'device_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeviceInfos()
    {
        return $this->hasMany(DeviceInfo::className(), ['device_id' => 'id']);
    }
}
