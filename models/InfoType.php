<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%info_type}}".
 *
 * @property integer $id
 * @property string $name
 *
 * @property DeviceInfo[] $deviceInfos
 * @property DeviceTypeInfoType[] $deviceTypeInfoTypes
 */
class InfoType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%info_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeviceInfos()
    {
        return $this->hasMany(DeviceInfo::className(), ['info_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeviceTypeInfoTypes()
    {
        return $this->hasMany(DeviceTypeInfoType::className(), ['info_type_id' => 'id']);
    }
}
