<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%device_info}}".
 *
 * @property integer $id
 * @property string $content
 * @property integer $info_type_id
 * @property integer $device_id
 *
 * @property Device $device
 * @property InfoType $infoType
 */
class DeviceInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%device_info}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content', 'info_type_id', 'device_id'], 'required'],
            [['info_type_id', 'device_id'], 'integer'],
            [['content'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'content' => Yii::t('app', 'Content'),
            'info_type_id' => Yii::t('app', 'Info type'),
            'device_id' => Yii::t('app', 'Device'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevice()
    {
        return $this->hasOne(Device::className(), ['id' => 'device_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInfoType()
    {
        return $this->hasOne(InfoType::className(), ['id' => 'info_type_id']);
    }
}
