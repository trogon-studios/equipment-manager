<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%device_type}}".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Device[] $devices
 * @property DeviceTypeInfoType[] $deviceTypeInfoTypes
 */
class DeviceType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%device_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevices()
    {
        return $this->hasMany(Device::className(), ['device_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeviceTypeInfoTypes()
    {
        return $this->hasMany(DeviceTypeInfoType::className(), ['device_type_id' => 'id']);
    }
}
