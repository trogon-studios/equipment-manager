<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%device_type_info_type}}".
 *
 * @property integer $id
 * @property integer $device_type_id
 * @property integer $info_type_id
 *
 * @property InfoType $infoType
 * @property DeviceType $deviceType
 */
class DeviceTypeInfoType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%device_type_info_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['device_type_id', 'info_type_id'], 'required'],
            [['device_type_id', 'info_type_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'device_type_id' => Yii::t('app', 'Device type'),
            'info_type_id' => Yii::t('app', 'Info type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInfoType()
    {
        return $this->hasOne(InfoType::className(), ['id' => 'info_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeviceType()
    {
        return $this->hasOne(DeviceType::className(), ['id' => 'device_type_id']);
    }
}
