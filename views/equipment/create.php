<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Device */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Device',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Equipment'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="equipment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
