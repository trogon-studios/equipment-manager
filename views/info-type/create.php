<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\InfoType */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Info Type',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Info Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="info-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
