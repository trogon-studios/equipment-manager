<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = Yii::t('app', 'About');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Menadżer sprzętu to prosta aplikacja internetowa pozwalająca na katalogowanie sprzętu komputerowego.
    </p>

	<div class="col-lg-4">
		<h2>Kontenery</h2>

		<p>Kontenery służą do tworzenia grup urządzeń, połączonych na długi czas ze sobą. Często również zmontowanych ze sobą.
		Najpopularniejszymi przykładami kontenerów to: komputery klasy PC, serwery itp. Dzięki nadaniu każdemu kontenerowi
		unikalnej nazwy, można w prosty sposób zidentyfikować go i dowiedzieć się z czego się składa, bez demontarzu.</p>
	</div>
	<div class="col-lg-4">
		<h2>Urządzenia</h2>

		<p>Urządzenia służą do identyfikowania najprostrzych urządzeń (nierozkładalnych). Są nimi zarówno części kontenerów
		takie jak: procesor czy pamięć RAM, jak również router czy projektor. Dzięki nadaniu każdemu urządzeniu unikatowej nazwy
		lub identyfikatora, jesteśmy w stanie sprawdzić jego parametry fizyczne jak i skatalogowaną konfigurację,
		bez kontaktu z urządzeniem.</p>
	</div>
	<div class="col-lg-4">
		<h2>Informacje</h2>

		<p>Informacje służą do opisywania skatalogowanych urządzeń. System umożliwia stworzenie dowolnych parametrów,
		według których można będzie opisać urządzenia zarówno opisujących jego właściwości fizyczne takie jak rozmiar czy lokalizację,
		jak również konfigurację oprogramowania. Do jednego urządzenia można utworzyć kilka wystąpień tego samego rodzaju informacji.</p>
	</div>
</div>
