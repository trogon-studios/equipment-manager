<?php
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Strona główna';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Manadżer sprzętu</h1>

        <p class="lead">Przejmij kontrolę nad sprzętem, który posiadasz.</p>

        <p><a class="btn btn-lg btn-success" href="<?= Url::toRoute('equipment/index') ?>">Rozpocznij zarządzanie</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Kontenery</h2>

                <p>Kontenery służą do tworzenia grup urządzeń, połączonych na długi czas ze sobą. Często również zmontowanych ze sobą.
				Najpopularniejszymi przykładami kontenerów to: komputery klasy PC, serwery itp. Dzięki nadaniu każdemu kontenerowi
				unikalnej nazwy, można w prosty sposób zidentyfikować go i dowiedzieć się z czego się składa, bez demontarzu.</p>
            </div>
            <div class="col-lg-4">
                <h2>Urządzenia</h2>

                <p>Urządzenia służą do identyfikowania najprostrzych urządzeń (nierozkładalnych). Są nimi zarówno części kontenerów
				takie jak: procesor czy pamięć RAM, jak również router czy projektor. Dzięki nadaniu każdemu urządzeniu unikatowej nazwy
				lub identyfikatora, jesteśmy w stanie sprawdzić jego parametry fizyczne jak i skatalogowaną konfigurację,
				bez kontaktu z urządzeniem.</p>
            </div>
            <div class="col-lg-4">
                <h2>Informacje</h2>

                <p>Informacje służą do opisywania skatalogowanych urządzeń. System umożliwia stworzenie dowolnych parametrów,
				według których można będzie opisać urządzenia zarówno opisujących jego właściwości fizyczne takie jak rozmiar czy lokalizację,
				jak również konfigurację oprogramowania. Do jednego urządzenia można utworzyć kilka wystąpień tego samego rodzaju informacji.</p>
            </div>
        </div>

    </div>
</div>
