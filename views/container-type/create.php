<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ContainerType */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Container Type',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Container Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
