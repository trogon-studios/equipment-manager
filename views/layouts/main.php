<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<link href="<?= Url::Base() ?>/favicon.ico" rel="icon" type="image/x-icon" />
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => Yii::t('app', 'Equipment manager'),
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
			$navitems = [
				['label' => Yii::t('app', 'Home'), 'url' => ['/site/index']],
			];
			if(Yii::$app->user->isGuest){
				$navitems[] = ['label' => Yii::t('app', 'Login'), 'url' => ['/site/login']];
				$navitems[] = ['label' => Yii::t('app', 'About'), 'url' => ['/site/about']];
			}else{
				$navitems[] = ['label' => Yii::t('app', 'Equipment'),
						'items' =>[
							['label' => Yii::t('app', 'Containers'), 'url' => ['/container']],
							['label' => Yii::t('app', 'Container types'), 'url' => ['/container-type']],
							['label' => Yii::t('app', 'Devices'), 'url' => ['/device']],
							['label' => Yii::t('app', 'Device infos'), 'url' => ['/device-info']],
							['label' => Yii::t('app', 'Device types'), 'url' => ['/device-type']],
							['label' => Yii::t('app', 'Info types'), 'url' => ['/info-type']],
						]];
				$navitems[] = ['label' => Yii::t('app', 'About'), 'url' => ['/site/about']];
				$navitems[] = ['label' => Yii::t('app', 'Logout ({username})', ['username' => Yii::$app->user->identity->username]),
						'url' => ['/site/logout'],
						'linkOptions' => ['data-method' => 'post']];
			}
			echo Nav::widget([
				'options' => ['class' => 'navbar-nav navbar-right'],
				'items' => $navitems
			]);
            NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; Maciej Klemarczyk 2014 - <?= date('Y') ?></p>
            <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
