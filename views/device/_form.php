<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Device */
/* @var $form yii\widgets\ActiveForm */
$containers_map = is_array($containers) ? ArrayHelper::map($containers, 'id', 'name') : [];
?>

<div class="device-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')
				->textInput(['maxlength' => 100]) ?>

	<?= $form->field($model, 'model')
				->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'device_type_id')
				->dropDownList(is_array($device_types) ? ArrayHelper::map($device_types, 'id', 'name') : []) ?>

    <?= $form->field($model, 'container_id')
				->dropDownList(ArrayHelper::merge(['' => Yii::t('app', '(not set)')], $containers_map)) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
