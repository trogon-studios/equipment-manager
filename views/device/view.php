<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Device */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Devices'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="device-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
			'model',
			[
				'label' => Yii::t('app', 'Container'),
				'format' => 'html',
				'value' => Html::a($model->container->name, Url::toRoute(['container/view', 'id' => $model->container->id])),
			],
            'deviceType.name:text:Device type',
        ],
    ]) ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create {modelClass}', ['modelClass' => 'Device Info']),
				['device-info/create', 'DeviceInfo[device_id]' => $model->id],
				['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $deviceInfo_dataProvider,
		//'filterModel' => new \app\models\DeviceInfo(),
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'infoType.name:text:Info type',
			'content',

            [
				'class' => 'yii\grid\ActionColumn',
				'controller' => 'device-info'
			],
        ],
    ]); ?>

</div>
