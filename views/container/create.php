<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Container */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Container',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Containers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'container_types' => $container_types,
    ]) ?>

</div>
