<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DeviceInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="device-info-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'device_id')
				->dropDownList(is_array($devices) ? ArrayHelper::map($devices, 'id', 'name') : []) ?>

    <?= $form->field($model, 'info_type_id')
				->dropDownList(is_array($info_types) ? ArrayHelper::map($info_types, 'id', 'name') : []) ?>

    <?= $form->field($model, 'content')
				->textInput(['maxlength' => 250]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
